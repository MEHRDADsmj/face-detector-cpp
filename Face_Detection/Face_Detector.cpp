#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <iostream>
#include <vector>

int main()
{
	cv::CascadeClassifier FaceCascade, EyeCascade;
	cv::VideoCapture Camera(0);
	cv::Mat CaptureImage, GrayScaleImage;
	std::string WindowName = "Face Detection";
	std::vector<cv::Rect> Face, Eye;

	if (!FaceCascade.load("haarcascade_frontalface_default.xml"))
	{
		std::cout << "Error loading face database" << std::endl;
		return 1;
	}
	if (!EyeCascade.load("haarcascade_eye.xml"))
	{
		std::cout << "Error loading eye database" << std::endl;
		return 1;
	}
	if (!Camera.isOpened())
	{
		std::cout << "Error accessing camera" << std::endl;
		return 1;
	}

	cv::namedWindow(WindowName, CV_WINDOW_FREERATIO);

	//Start Face Detection process
	while (cv::waitKey(33) != 27)
	{
		Camera >> CaptureImage;
		cv::cvtColor(CaptureImage, GrayScaleImage, CV_RGB2GRAY);
		cv::equalizeHist(GrayScaleImage, GrayScaleImage);
		FaceCascade.detectMultiScale(GrayScaleImage, Face, 1.1, 3, 0, cvSize(0, 0), cvSize(300, 300));

		if (Face.size())
		{
			putText(CaptureImage, "Face Detected", cv::Point(30, 30), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255, 0, 0));
		}

		for (int FaceId = 0; FaceId < Face.size(); FaceId++)
		{
			cv::Point UpperLeft(Face[FaceId].x, Face[FaceId].y);
			cv::Point LowerRight(Face[FaceId].x + Face[FaceId].width, Face[FaceId].y + Face[FaceId].height);

			cv::rectangle(CaptureImage, UpperLeft, LowerRight, CV_RGB(0, 0, 255));
			
			//Eye detection
			cv::Mat EyeImage = GrayScaleImage(Face[FaceId]);
			EyeCascade.detectMultiScale(EyeImage, Eye, 1.1, 3, 0, cvSize(0, 0), cvSize(50, 50));
			for (int EyeId = 0; EyeId < Eye.size(); EyeId++)
			{
				cv::Point EyeUpperLeft(Face[FaceId].x + Eye[EyeId].x, Face[FaceId].y + Eye[EyeId].y);
				cv::Point EyeLowerRight(Face[FaceId].x + Eye[EyeId].x + Eye[EyeId].width, Face[FaceId].y + Eye[EyeId].y + Eye[EyeId].height);
				cv::rectangle(CaptureImage, EyeUpperLeft, EyeLowerRight, CV_RGB(0, 255, 0));
			}
		}

		cv::imshow(WindowName, CaptureImage);
	}

	return 0;
}